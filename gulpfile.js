var gulp = require("gulp"),
    connect = require("gulp-connect"),
    opn = require("opn"),
    csso = require('gulp-csso'),
    autoprefixer = require('gulp-autoprefixer'),
    plumber = require('gulp-plumber'),
    slim = require('gulp-slim'),
    sass = require("gulp-sass"),
    coffee = require('gulp-coffee'),
    concat = require('gulp-concat');

//***********************************//
// 1.Локальный сервер и автообновление //
//***********************************//

// Запуск локального сервера
gulp.task('connect', function(){
    connect.server({
        root: 'app',
        livereload: true,
        port: 8888
    });
    opn('http://localhost:8888');
});

// Слежение за HTML файлами
gulp.task('html', function() {
    gulp.src('./app/index.html')
        .pipe(connect.reload());
});

// Слежение за CSS файлами
gulp.task('css', function() {
    gulp.src('./app/css/app.css')
        .pipe(plumber())
        .pipe(connect.reload());
});

// Слежение за JS файлами
gulp.task('js', function() {
    gulp.src('./app/js/app.js')
        .pipe(connect.reload());
});

// Компиляция SASS
gulp.task('sass', function (){
    return gulp.src('./app/dev/sass/app.scss')
        .pipe(plumber())
        .pipe(sass({outputStyle: "compressed"}).on('error', sass.logError))
        .pipe(autoprefixer(
            'last 7 version',
            '> 1%',
            'ie 8',
            'ie 9',
            'ios 6',
            'android 4'
        ))
        .pipe(csso())
        .pipe(gulp.dest('./app/css'));
});

// Компиляция Slim для index.slim
gulp.task('slim-index', function(){
    gulp.src("./app/dev/slim/index.slim")
        .pipe(plumber())
        .pipe(slim({
            pretty: true
        }))
        .pipe(gulp.dest("./app/"));
});

// Компиляция Slim для шаблонов AngularJS
gulp.task('slim-templates', function(){
    gulp.src("./app/dev/slim/tpl/**/*.slim")
        .pipe(plumber())
        .pipe(slim({
            pretty: true
        }))
        .pipe(gulp.dest("./app/tpl"));
});

// Конкатенация Coffee-script
gulp.task('coffee-concat', function() {
    gulp.src('./app/dev/coffee/angular/**/*.coffee')
        .pipe(concat('app.coffee'))
        .pipe(gulp.dest('./app/dev/coffee'));
});

// Компиляция Coffee-script
gulp.task('coffee', ['coffee-concat'], function() {
    gulp.src('./app/dev/coffee/app.coffee')
        .pipe(plumber())
        .pipe(coffee({
            bare: true
        }))
        .pipe(gulp.dest('./app/js'));
});

// Запуск слежения
gulp.task('watch', function() {
   gulp.watch(['./app/*.html'], ['html']);
   gulp.watch(['./app/css/*.css'], ['css']);
   gulp.watch(['./app/js/*.js'], ['js']);
   gulp.watch(['./app/dev/slim/index.slim'], ['slim-index']);
   gulp.watch(['./app/dev/slim/tpl/**/*.slim'], ['slim-templates']);
   gulp.watch(['./app/dev/sass/*.scss'], ['sass']);
   gulp.watch(['./app/dev/coffee/**/*.coffee'], ['coffee']);
});

//***********************************//
// /1.Локальный сервер и автообновление//
//***********************************//

// Зачада по умолчанию
gulp.task('default',['connect', 'watch']);