app = angular.module('ngRequests', ['ngStorage'])
app.controller('mainController', ['$scope', '$localStorage',
  mainController = ($scope, $localStorage) ->
    $scope.$storage = $localStorage.$default({
      "predicate":"title","projects":[{"id":"1","name":"First project"},{"id":"2","name":"Second project"},{"id":"3","name":"Another project"}],"requests":[{"id":"0","title":"First request","text":"lorem","project":"First project","urgency":"High","date":1458076056815},{"id":"1","title":"Second request","text":"pixel","project":"Another project","urgency":"Middle","date":1457086026815}],"reverse":false
    })
    $scope.order = (predicate) ->
      $scope.$storage.reverse = if $scope.$storage.predicate == predicate then !$scope.$storage.reverse else false
      $scope.$storage.predicate = predicate
      return
    $scope.reset = () ->
      $localStorage.$reset()
    $scope.deleteRequest = (request) ->
      index = $scope.$storage['requests'].indexOf(request)
      $scope.$storage['requests'].splice(index, 1)
      return
    $scope.formRequest = {}
    $scope.addRequest = () ->
      formRequest =
        id: $scope.$storage['requests'].length
        title: $scope.formRequest.title
        text: $scope.formRequest.text
        project: $scope.formRequest.project
        urgency: $scope.formRequest.urgency
        date: new Date()
      $scope.$storage.requests.push(formRequest)
      $scope.formRequest = {}
      return
    return
])